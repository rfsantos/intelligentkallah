/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.logic;

import ai.kallah.intelligence.DefaultEvaluationFunction;
import ai.kallah.intelligence.EvaluationFunction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public final class Logics {

    private KallahState currentState;

    public Logics() {

        restartBoard();
    }

    public void restartBoard() {

        this.currentState = new KallahState();
        for (byte i = 1; i < 7; i++) {

            this.currentState.setPits(new KallahEntry((byte) 1, i), (byte) 3);
            this.currentState.setPits(new KallahEntry((byte) 2, i), (byte) 3);
        }
    }

    public KallahState nextState(KallahEntry action) {

        return nextState(currentState, action);
    }

    public List<KallahState> nextStates(byte player) {

        return nextStates(currentState, player);
    }

    public List<KallahState> nextStates(char player) {

        return nextStates(getBytePlayer(player));
    }

    public KallahState makeMove(KallahEntry action) {

        this.currentState = nextState(action);
        return new KallahState(currentState);
    }

    public KallahState getCurrentState() {

        return new KallahState(currentState);
    }

    public void setCurrentState(KallahState currentState) {
        this.currentState = currentState;
    }

    public static List<KallahState> nextStates(KallahState state, byte player) {
        List<KallahState> nextStates = new LinkedList<>();

        for (KallahEntry action : getPossibleActions(state, player)) {
            nextStates.add(nextState(state, action));
        }

        return nextStates;
    }

    public static List<KallahEntry> getPossibleActions(KallahState state, byte player) {

        List<KallahEntry> possibleActions = new ArrayList<>(7);
        KallahEntry currentAction = new KallahEntry(player, (byte) 1);

        for (byte i = 1; i < 7; i++) {

            currentAction.setPosition(i);
            if (state.getPits(currentAction) > 0) {
                possibleActions.add(new KallahEntry(player, i));
            }
        }

        return possibleActions;
    }

    public static KallahState nextState(KallahState state, KallahEntry action) {

        KallahState nextState = new KallahState(action, state);
        KallahEntry lastPosition = action;

        for (byte i = 0; i < nextState.getPits(action); i++) {

            lastPosition = nextState.getNextPosition(lastPosition);

            if (nextState.isScorePosition(lastPosition) && lastPosition.getPlayer() != action.getPlayer()) {
                continue; // Can't add pits to the opponent's score
            }
            nextState.addPits(lastPosition, (byte) 1);
        }

        nextState.setPits(action, (byte) 0);

        if (getOpponentPits(nextState, action, lastPosition)) {

            byte playersPits = nextState.removePits(lastPosition);
            KallahEntry opponentNeighbor = nextState.getOpponentNeighbor(lastPosition);
            byte opponentsPits = nextState.removePits(opponentNeighbor);
            nextState.addPits(nextState.getScorePosition(action), (byte) (playersPits + opponentsPits));
        }

        if (!(hasOneMoreTurn(nextState, action, lastPosition))) {

            nextState.switchPlayer(action);
        }

        if (nextState.isFinalState()) {

            nextState.scoreAll();
        }

        return nextState;
    }

    private static boolean hasOneMoreTurn(KallahState endState, KallahEntry action, KallahEntry lastPosition) {

        return endState.isScorePosition(lastPosition) && action.getPlayer() == lastPosition.getPlayer();
    }

    private static boolean getOpponentPits(KallahState endState, KallahEntry action, KallahEntry lastPosition) {

        return endState.getNextPlayer() == action.getPlayer() && !endState.isScorePosition(lastPosition) && endState.getPits(lastPosition) == 1 && endState.getPits(endState.getOpponentNeighbor(lastPosition)) > 0 && lastPosition.getPlayer() == action.getPlayer();
    }

    public static class KallahEntry {

        private byte player, position;

        public KallahEntry(byte player, byte position) {

            checkValidEntry(player, position);
            this.player = player;
            this.position = position;
        }

        public KallahEntry(char player, byte position) {

            byte bytePlayer = getBytePlayer(player);
            checkValidEntry(bytePlayer, position);
            this.player = bytePlayer;
            this.position = position;
        }

        public byte getPlayer() {
            return player;
        }

        public void setPlayer(byte player) {
            this.player = player;
        }

        public byte getPosition() {
            return position;
        }

        public void setPosition(byte position) {
            this.position = position;
        }

        private void checkValidEntry(byte player, byte position) {

            checkValidPlayer(player);
            checkValidPosition(position);
        }

        private void checkValidPlayer(byte player) {

            if (player < 1 || player > 2) {

                throw new IllegalStateException("Not a valid player");
            }
        }

        private void checkValidPosition(byte position) {

            if (position < 1 || position > 7) {

                throw new IllegalStateException("Not a valid board position");
            }
        }

        @Override
        public String toString() {

            return "" + getCharPlayer(player) + position;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 89 * hash + this.player;
            hash = 89 * hash + this.position;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final KallahEntry other = (KallahEntry) obj;
            if (this.player != other.player) {
                return false;
            }
            return this.position == other.position;
        }
    }

    private static byte getBytePlayer(char player) {

        switch (player) {

            case 'L':
            case 'l':
                return 1;
            case 'R':
            case 'r':
                return 2;
            default:
                return 0;
        }
    }

    private static byte getCharPlayer(byte player) {

        switch (player) {

            case 1:
                return 'L';
            case 2:
                return 'R';
            default:
                return 'E';
        }
    }

    public static final class KallahState implements Cloneable {

        private final byte[][] board;
        private byte nextPlayer;
        private EvaluationFunction evalFunction;
        private KallahEntry lastPlay;

        public KallahState() {

            this.board = new byte[2][7];
            this.nextPlayer = 1;
            this.evalFunction = new DefaultEvaluationFunction();
        }

        public KallahState(KallahEntry lastPlay, KallahState clone) {

            this.board = copyBoard(clone.board);
            this.nextPlayer = clone.nextPlayer;
            this.evalFunction = clone.evalFunction;
            this.lastPlay = lastPlay;
        }

        public KallahState(KallahState clone) {

            this(clone.lastPlay, clone);
        }

        private byte[][] copyBoard(byte[][] board) {

            byte[][] newBoard = new byte[2][7];
            for (int i = 0; i < 2; i++) {
                System.arraycopy(board[i], 0, newBoard[i], 0, 7);
            }

            return newBoard;
        }

        private void addPits(KallahEntry entry, byte numPits) {

            this.board[entry.player - 1][entry.position - 1] += numPits;
        }

        private void setPits(KallahEntry entry, byte numPits) {

            this.board[entry.player - 1][entry.position - 1] = numPits;
        }

        public byte getPits(KallahEntry entry) {

            return this.board[entry.player - 1][entry.position - 1];
        }

        private byte removePits(KallahEntry entry) {

            byte pits = getPits(entry);
            this.board[entry.player - 1][entry.position - 1] = 0;
            return pits;
        }

        public byte getScore(KallahEntry entry) {

            return getPits(new KallahEntry(entry.getPlayer(), (byte) 7));
        }

        public byte getNextPlayer() {
            return nextPlayer;
        }

        private void switchToOpponentPlayer(KallahEntry entry) {

            entry.setPlayer((byte) (entry.player % 2 + 1));
        }

        private void switchPlayer(KallahEntry entry) {

            this.nextPlayer = (byte) (entry.player % 2 + 1);
        }

        public boolean isScorePosition(KallahEntry entry) {

            return entry.position - 1 == 6;
        }

        public KallahEntry getScorePosition(KallahEntry entry) {

            return new KallahEntry(entry.player, (byte) 7);
        }

        public KallahEntry getOpponentNeighbor(KallahEntry entry) {

            if (isScorePosition(entry)) {

                return null;
            }

            switchToOpponentPlayer(entry);
            KallahEntry neighbor = new KallahEntry(entry.getPlayer(), (byte) (7 - entry.position));
            switchToOpponentPlayer(entry); //re-switch players to the original configuration
            return neighbor;
        }

        public KallahEntry getNextPosition(KallahEntry entry) {

            if (isScorePosition(entry)) {

                return new KallahEntry((byte) (entry.player % 2 + 1), (byte) 1);
            } else {

                return new KallahEntry(entry.player, (byte) (entry.position + 1));
            }
        }

        public boolean isFinalState() {

            boolean isFinal1 = true, isFinal2 = true;
            for (byte i = 0; i < 6; i++) {

                if (board[0][i] > 0 && board[1][i] > 0) {
                    return false;
                }

                if (board[0][i] > 0) {

                    isFinal1 = false;
                }

                if (board[1][i] > 0) {

                    isFinal2 = false;
                }
            }

            return isFinal1 || isFinal2;
        }

        private void scoreAll() {

            for (byte i = 0; i < 6; i++) {

                this.board[0][6] += this.board[0][i];
                this.board[0][i] = 0;

                this.board[1][6] += this.board[1][i];
                this.board[1][i] = 0;
            }
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 71 * hash + Arrays.hashCode(this.board);
            hash = 71 * hash + this.nextPlayer;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final KallahState other = (KallahState) obj;
            if (!Arrays.equals(this.board, other.board)) {
                return false;
            }
            return this.nextPlayer == other.nextPlayer;
        }

        @Override
        public String toString() {

            return printBoard() + " " + getCharPlayer(nextPlayer);
        }

        public String printBoard() {

            StringBuilder builder = new StringBuilder();
            for (byte i = 0; i < 2; i++) {
                for (byte k = 0; k < 7; k++) {
                    builder.append(board[i][k]);
                    builder.append(" ");
                }
            }

            builder.deleteCharAt(builder.length() - 1);
            return builder.toString();
        }

        public int evaluate() {

            return getEvalFunction().evaluate(this);
        }

        public EvaluationFunction getEvalFunction() {
            return evalFunction;
        }

        public void setEvalFunction(EvaluationFunction evalFunction) {
            this.evalFunction = evalFunction;
        }

        public KallahEntry getLastPlay() {
            return lastPlay;
        }

        private void setLastPlay(KallahEntry lastPlay) {
            this.lastPlay = lastPlay;
        }

        @Override
        protected Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(Logics.class.getName()).log(Level.SEVERE, null, ex);
            }

            return null;
        }
    }
}
