/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.game;

import ai.kallah.logic.Logics;

/**
 *
 * @author rafael
 */
public class KallahGUIPlayer implements KallahPlayer, KallahPlayObserver {

    private Logics.KallahEntry play = null;
    private byte playerNumber;

    @Override
    public void setPlayerNumber(byte player) {

        this.playerNumber = player;
    }

    @Override
    public synchronized Logics.KallahEntry makePlay() {

        this.play = null;
        while(this.play == null){
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
        return this.play;
    }

    @Override
    public void readPlay(Logics.KallahState state) {
    }

    @Override
    public synchronized void update(Logics.KallahEntry kallahPlay) {

        if (kallahPlay.getPlayer() == this.playerNumber && this.play == null) {
            this.play = kallahPlay;
            notifyAll();
        }
    }

}
