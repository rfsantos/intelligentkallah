/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.game;

/**
 *
 * @author rafael
 */
public class InvalidKallahActionException extends RuntimeException {

    public InvalidKallahActionException(String message) {
        super(message);
    }

    public InvalidKallahActionException(String message, Throwable cause) {
        super(message, cause);
    }
}
