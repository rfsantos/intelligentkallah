/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.game;

import ai.kallah.intelligence.GuaranteedPointsEvaluationFunction;
import ai.kallah.intelligence.MaxPointsEvaluationFunction;
import ai.kallah.intelligence.MinimaxPlayer;

/**
 *
 * @author rafael
 */
public class Run {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        new KallahGame(new KallahGUIPlayer(), new KallahGUIPlayer()).startGame();
       // new KallahGame(new KallahGUIPlayer(), new KallahGUIPlayer()).startGame();
    }
}
