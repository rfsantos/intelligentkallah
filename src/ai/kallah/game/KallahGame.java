/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.game;

import ai.kallah.logic.Logics;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class KallahGame {

    private KallahPlayer player1, player2;
    private Logics gameLogics = null;
    private final KallahBoard board;

    public KallahGame(KallahPlayer player1, KallahPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.board = new KallahBoard();
    }

    public void startGame() {

        this.gameLogics = new Logics();
        setPlayers();
        board.updateBoard(this.gameLogics.getCurrentState());
        board.setVisible(true);
        runGame();
    }

    private void runGame() {

        while (!this.gameLogics.getCurrentState().isFinalState()) {
            Logics.KallahEntry nextPlayerMove = getNextPlayer().makePlay();
            Logics.KallahState nextState = this.gameLogics.makeMove(nextPlayerMove);
            printPlay(nextPlayerMove);
            this.player1.readPlay(nextState);
            this.player2.readPlay(nextState);
            this.board.updateBoard(nextState);
        }

        this.board.updateBoard(this.gameLogics.getCurrentState());
    }

    private void printPlay(Logics.KallahEntry play) {

        System.out.println("Player: " + play.getPlayer() + " Position: " + play.getPosition());
    }

    private void setPlayers() {

        this.player1.setPlayerNumber((byte) 1);
        if (player1 instanceof KallahPlayObserver) {
            this.board.addObserver((KallahPlayObserver) player1);
        }
        this.player1 = new TimedPlayerAdapter(player1);

        this.player2.setPlayerNumber((byte) 2);
        if (player2 instanceof KallahPlayObserver) {
            this.board.addObserver((KallahPlayObserver) player2);
        }
        this.player2 = new TimedPlayerAdapter(player2);
    }

    private KallahPlayer getNextPlayer() {

        if (gameLogics.getCurrentState().getNextPlayer() == 1) {
            return player1;
        } else if (gameLogics.getCurrentState().getNextPlayer() == 2) {
            return player2;
        }

        return null;
    }

    private KallahPlayer getOpponent() {

        return getNextPlayer().equals(player1) ? player2 : player1;
    }

    private class TimedPlayerAdapter implements KallahPlayer, Runnable {

        private final KallahPlayer player;
        private final int MINUTES = 1;
        private final long TIME_LIMIT = MINUTES * 60 * 1000;
        private Logics.KallahEntry currentPlay;

        public TimedPlayerAdapter(KallahPlayer player) {
            this.player = player;
        }

        @Override
        public void setPlayerNumber(byte playerNumber) {

            this.player.setPlayerNumber(playerNumber);
        }

        @Override
        public Logics.KallahEntry makePlay() {
            this.currentPlay = null;
            long startTime = System.currentTimeMillis();
            Thread play = new Thread(this);
            play.start();
            try {
                while (this.currentPlay == null && play.isAlive()) {

                    play.join(1000);

                    if ((System.currentTimeMillis() - startTime) > TIME_LIMIT && play.isAlive()) {
                        play.interrupt();
                        play.join();
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(KallahGame.class.getName()).log(Level.SEVERE, null, ex);
            }

            return this.currentPlay;
        }

        @Override
        public void readPlay(Logics.KallahState state) {

            this.player.readPlay(state);
        }

        @Override
        public void run() {

            this.currentPlay = this.player.makePlay();
        }

    }
}
