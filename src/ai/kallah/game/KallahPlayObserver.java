/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.game;

import ai.kallah.logic.Logics;

/**
 *
 * @author rafael
 */
public interface KallahPlayObserver {

    public void update(Logics.KallahEntry kallahPlay);
}
