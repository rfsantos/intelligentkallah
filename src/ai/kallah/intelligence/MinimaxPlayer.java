/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.intelligence;

import ai.kallah.game.KallahPlayer;
import ai.kallah.logic.Logics;

/**
 *
 * @author rafael
 */
public class MinimaxPlayer implements KallahPlayer {

    private final Logics kallahBoard;
    private EvaluationFunction evalFunction;
    private int maxDepth, alpha, beta;
    private byte player;

    public MinimaxPlayer() {

        this(new MaxPointsEvaluationFunction((byte) 1));
    }

    public MinimaxPlayer(EvaluationFunction evalFunction) {
        this.kallahBoard = new Logics();
        this.evalFunction = evalFunction;
    }

    @Override
    public void setPlayerNumber(byte playerNumber) {

        this.player = playerNumber;
        this.evalFunction.setPlayer(playerNumber);
    }

    @Override
    public Logics.KallahEntry makePlay() {

        this.maxDepth = 11;
        this.kallahBoard.getCurrentState().setEvalFunction(evalFunction);
        return argmax(kallahBoard.getCurrentState());
    }

    @Override
    public void readPlay(Logics.KallahState state) {

        this.kallahBoard.setCurrentState(state);
    }

    private Logics.KallahEntry argmax(Logics.KallahState state) {

        this.alpha = Integer.MIN_VALUE;
        this.beta = Integer.MAX_VALUE;
        Logics.KallahEntry bestMove = null;
        int max = Integer.MIN_VALUE;

        for (Logics.KallahState nextState : Logics.nextStates(state, player)) {

            int vMax = max(nextState, 0);
            if (vMax > max) {
                max = vMax;
                bestMove = nextState.getLastPlay();
            }
        }

        System.out.println("Max: " + max);
        return bestMove;
    }

    private int max(Logics.KallahState state, int depth) {

//        if (Thread.interrupted()) {
//            this.maxDepth = depth;
//        }

        int max = Integer.MIN_VALUE;
        if (state.isFinalState() || depth >= this.maxDepth) {

            state.setEvalFunction(evalFunction);
            return state.evaluate();
        } else {

            for (Logics.KallahState nextState : Logics.nextStates(state, state.getNextPlayer())) {

                max = Math.max(max, nextState.getNextPlayer() == state.getNextPlayer() ? max(nextState, depth + 1) : min(nextState, depth + 1));
//                if (max >= beta) {
//                    break;
//                }
            }

//            alpha = Math.max(alpha, max);
        }

        return max;
    }

    private int min(Logics.KallahState state, int depth) {

//        if (Thread.interrupted()) {
//            this.maxDepth = depth;
//        }

        int min = Integer.MAX_VALUE;

        if (state.isFinalState() || depth >= this.maxDepth) {

            state.setEvalFunction(evalFunction);
            return state.evaluate();
        } else {

            for (Logics.KallahState nextState : Logics.nextStates(state, state.getNextPlayer())) {

                min = Math.min(min, nextState.getNextPlayer() == state.getNextPlayer() ? min(nextState, depth + 1) : max(nextState, depth + 1));
//                if (min <= alpha) {
//                    break;
//                }
            }

//            beta = Math.min(beta, min);
        }

        return min;
    }
}
