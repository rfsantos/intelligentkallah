/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.intelligence;

import ai.kallah.logic.Logics;

/**
 *
 * @author rafaelfernando
 */
public class GuaranteedPointsEvaluationFunction extends EvaluationFunction {

    @Override
    public int evaluate(Logics.KallahState state) {

        int guaranteedPlayer = calculateGuaranteedPoints(getPlayer(), state);
        int guaranteedOpponent = calculateGuaranteedPoints(getOpponent(), state);

        return guaranteedPlayer - guaranteedOpponent;
    }

    private int calculateGuaranteedPoints(byte player, Logics.KallahState currentState) {

        int guaranteedPoints = 0;
        for (int i = 1; i < 7; i++) {

            int maxPoints = Math.min(7 - i, currentState.getPits(new Logics.KallahEntry(player, (byte) i)));
            if (maxPoints == 0) {
                continue;
            }

            for (int j = i+1; 1 < 7 - j; j++) {

                if (currentState.getPits(currentState.getOpponentNeighbor(new Logics.KallahEntry(player, (byte) j))) == 0) {

                    for (int k = j + 1; 1 < 7 - k; k++) {

                        Logics.KallahEntry opponentNeighbor = currentState.getOpponentNeighbor(new Logics.KallahEntry(player, (byte) k));
                        if (currentState.getPits(opponentNeighbor) < opponentNeighbor.getPosition()) {

                            maxPoints--;
                            break;
                        }
                    }
                }
            }

            guaranteedPoints += maxPoints;
        }

        return guaranteedPoints + currentState.getScore(new Logics.KallahEntry(player, player));
    }

}
