/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.intelligence;

import ai.kallah.logic.Logics;
import java.util.List;

/**
 *
 * @author rafael
 */
public class MaxPointsEvaluationFunction extends EvaluationFunction {

    public MaxPointsEvaluationFunction(byte player) {
        this.setPlayer(player);
    }

    @Override
    public int evaluate(Logics.KallahState state) {

        List<Logics.KallahState> playerStates = Logics.nextStates(state, state.getNextPlayer());
        //List<Logics.KallahState> playerStates = Logics.nextStates(state, state.getNextPlayer());
        //List<Logics.KallahState> opponentStates = Logics.nextStates(state, opponent);

        int maxPlayer = sumPoints(getPlayer(), state, playerStates);
        //int maxOpponent = sumPoints(opponent, state, opponentStates);
        int maxOpponent = sumPoints(getOpponent(), state, playerStates);
        
        return maxPlayer - maxOpponent;
    }

    private int sumPoints(byte currentPlayer, Logics.KallahState currentState, List<Logics.KallahState> nextStates) {
        
        int points = 0;
        
        points += currentState.getScore(new Logics.KallahEntry(currentPlayer, (byte) 1)); //Add current points
        for(Logics.KallahState nextState: nextStates){
            
            points += nextState.getScore(new Logics.KallahEntry(currentPlayer, (byte) 1)); //Add possible next points
            
            if(currentPlayer == currentState.getNextPlayer() && currentPlayer == nextState.getNextPlayer()){
                
                points += sumPoints(currentPlayer, nextState, Logics.nextStates(nextState, currentPlayer)); // While it is the player's turn
            }
        }
        
        return points;
    }
}
