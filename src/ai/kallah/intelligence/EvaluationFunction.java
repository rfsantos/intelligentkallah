/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.kallah.intelligence;

import ai.kallah.logic.Logics;

/**
 *
 * @author rafael
 */
public abstract class EvaluationFunction implements Cloneable {

    private byte player, opponent;

    public abstract int evaluate(Logics.KallahState state);

    public byte getPlayer() {
        return player;
    }

    public void setPlayer(byte player) {
        this.player = player;
        this.opponent = (byte) ((byte) (player % 2) + 1);
    }

    public byte getOpponent() {
        return opponent;
    }
}
